use crate::scanner::Scanner;
use env_logger::Env;
use std::env;
use std::io::Write;
use std::process;
mod scanner;
fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("warn")).init();

    let args: Vec<String> = env::args().collect();
    match args.len() {
        1 => run_prompt(),
        2 => run_file(&args[1]),
        _ => {
            println!("Usage: jlox [script]");
            process::exit(64)
        }
    }
}

/// Reads the file from path and executes it
fn run_file(path: &str) {
    let src_code = std::fs::read_to_string(path).unwrap();
    run(src_code)
}

/// Fire up the REPL prompt to execute code one line at a time
fn run_prompt() {
    // Read input from stdin as a string and convert to vec of lines
    // for each line in input:
    // if (line == null) break;
    // run(line);
    loop {
        print!("> ");
        std::io::stdout().flush().unwrap();
        let mut line = String::new();
        std::io::stdin()
            .read_line(&mut line)
            .expect("Unable to read line from the REPL");
        if line.is_empty() {
            break;
        }
        // VM run the line
        run(line)
    }
}

fn run(src_code: String) {
    // Create a new Scanner
    let mut scanner = Scanner::new(src_code);

    scanner.scan_tokens();
}
