use log::debug;
use phf::phf_map;
use std::fmt;

#[derive(Debug, Clone)]
pub enum TokenType {
    // Single-character tokens.
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,

    // One or two character tokens.
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals.
    Identifier,
    String { literal: String },
    Number { literal: f64 },

    // Keywords.
    And,
    Class,
    Else,
    False,
    For,
    Fun,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,

    Error,
    Eof,
}

// Premature optimization is the root of all evil but this crate seemed fun
static KEYWORDS: phf::Map<&'static str, TokenType> = phf_map! {
"and" => TokenType::And,
"class" => TokenType::Class,
"else" => TokenType::Else,
"false" => TokenType::False,
"for" => TokenType::For,
"fun" => TokenType::Fun,
"if" => TokenType::If,
"nil" => TokenType::Nil,
"or" => TokenType::Or,
"print" => TokenType::Print,
"return" => TokenType::Return,
"super" => TokenType::Super,
"this" => TokenType::This,
"true" => TokenType::True,
"var" => TokenType::Var,
"while" => TokenType::While,
};

// TODO Do I need whatever `literal` is as a field?
#[derive(Debug)]
pub struct Token {
    pub token_type: TokenType,
    pub lexeme: String,
    pub line: usize,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.token_type {
            TokenType::String { literal } => write!(f, "{} {} {}", self.token_type, self.lexeme, literal),
            TokenType::Number { literal } => write!(f, "{} {} {literal:.1}", self.token_type, self.lexeme, literal=literal),
            _ => write!(f, "{} {} null", self.token_type, self.lexeme)
        }
    }
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            TokenType::LeftParen => write!(f, "LEFT_PAREN"),
            TokenType::RightParen => write!(f, "RIGHT_PAREN"),
            TokenType::LeftBrace => write!(f, "LEFT_BRACE"),
            TokenType::RightBrace => write!(f, "RIGHT_BRACE"),
            TokenType::Comma => write!(f, "COMMA"),
            TokenType::Dot => write!(f, "DOT"),
            TokenType::Minus => write!(f, "MINUS"),
            TokenType::Plus => write!(f, "PLUS"),
            TokenType::Semicolon => write!(f, "SEMICOLON"),
            TokenType::Slash => write!(f, "SLASH"),
            TokenType::Star => write!(f, "STAR"),
            TokenType::Bang => write!(f, "BANG"),
            TokenType::BangEqual => write!(f, "BANG_EQUAL"),
            TokenType::Equal => write!(f, "EQUAL"),
            TokenType::EqualEqual => write!(f, "EQUAL_EQUAL"),
            TokenType::Greater => write!(f, "GREATER"),
            TokenType::GreaterEqual => write!(f, "GREATER_EQUAL"),
            TokenType::Less => write!(f, "LESS"),
            TokenType::LessEqual => write!(f, "LESS_EQUAL"),
            TokenType::Identifier => write!(f, "IDENTIFIER"),
            TokenType::String {literal: _} => write!(f, "STRING"),
            TokenType::Number{literal: _} => write!(f, "NUMBER"),
            TokenType::And => write!(f, "AND"),
            TokenType::Class => write!(f, "CLASS"),
            TokenType::Else => write!(f, "ELSE"),
            TokenType::False => write!(f, "FALSE"),
            TokenType::For => write!(f, "FOR"),
            TokenType::Fun => write!(f, "FUN"),
            TokenType::If => write!(f, "IF"),
            TokenType::Nil => write!(f, "NIL"),
            TokenType::Or => write!(f, "OR"),
            TokenType::Print => write!(f, "PRINT"),
            TokenType::Return => write!(f, "RETURN"),
            TokenType::Super => write!(f, "SUPER"),
            TokenType::This => write!(f, "THIS"),
            TokenType::True => write!(f, "TRUE"),
            TokenType::Var => write!(f, "VAR"),
            TokenType::While => write!(f, "WHILE"),
            TokenType::Error => write!(f, "ERROR"),
            TokenType::Eof => write!(f, "EOF"),
        }
    }
}

pub struct Scanner {
    pub source: String,
    pub start: usize,
    pub current: usize,
    pub line: usize,
    pub tokens: Vec<Token>,
}

impl Scanner {
    pub fn new(code: String) -> Scanner {
        Scanner {
            source: code,
            current: 0,
            start: 0,
            line: 1,
            tokens: Vec::new(),
        }
    }
    pub fn scan_tokens(&mut self) {
        while !self.is_at_end() {
            self.skip_whitespace();
            // we are at the beginning of the next lexeme
            self.start = self.current;
            self.scan_token();
        }
        let new_token = self.create_token(TokenType::Eof);
        println!("{}  null", TokenType::Eof);
        self.tokens.push(new_token);
        debug!("Tokens List: {:#?}", self.tokens);
    }

    pub fn number(&mut self) {
        while is_digit(self.peek()) {
            self.advance();
        }
        if self.peek() == b'.' && is_digit(self.peek_next()) {
            self.advance();
            while is_digit(self.peek()) {
                self.advance();
            }
        }

        let number: f64 = self.lexeme().parse().unwrap();
        self.add_token(TokenType::Number{literal: number})
    }

    /// Grabs the text of the current lexeme and creates a new token for it
    pub fn create_token(&mut self, token: TokenType) -> Token {
        Token {
            token_type: token,
            lexeme: self.lexeme().to_string(),
            line: self.line,
        }
    }

    /// Gets the lexeme for a token that is being created
    pub fn lexeme(&self) -> &str {
        &self.source[self.start..self.current]
    }

    // TODO Separate scanning a token and creating it from appending it to the tokens vec
    /// scans a token and adds it to the tokens list
    pub fn scan_token(&mut self) {
        match self.advance() {
            b'(' => self.add_token(TokenType::LeftParen),
            b')' => self.add_token(TokenType::RightParen),
            b'{' => self.add_token(TokenType::LeftBrace),
            b'}' => self.add_token(TokenType::RightBrace),
            b';' => self.add_token(TokenType::Semicolon),
            b',' => self.add_token(TokenType::Comma),
            b'.' => self.add_token(TokenType::Dot),
            b'-' => self.add_token(TokenType::Minus),
            b'+' => self.add_token(TokenType::Plus),
            b'/' if self.next_char_matches(b'/') => {
                while self.peek() != b'\n' && !self.is_at_end() {
                    self.advance();
                }
            }
            b'/' => self.add_token(TokenType::Slash),
            b'*' => self.add_token(TokenType::Star),
            b'!' if self.next_char_matches(b'=') => self.add_token(TokenType::BangEqual),
            b'!' => self.add_token(TokenType::Bang),
            b'=' if self.next_char_matches(b'=') => self.add_token(TokenType::EqualEqual),
            b'=' => self.add_token(TokenType::Equal),
            b'<' if self.next_char_matches(b'=') => self.add_token(TokenType::LessEqual),
            b'<' => self.add_token(TokenType::Less),
            b'>' if self.next_char_matches(b'=') => self.add_token(TokenType::GreaterEqual),
            b'>' => self.add_token(TokenType::Greater),
            b'"' => self.string(),
            c if is_digit(c) => self.number(),
            c if is_alpha(c) => self.identifier(),
            _ => self.error_token("Unexpected character."), // this seems to also trigger on EOF
        }
    }
    pub fn is_at_end(&mut self) -> bool {
        self.current == self.source.len() - 1 // TODO: only works with the -1 for some reason?
    }

    pub fn string(&mut self) {
        while self.peek() != b'"' && !self.is_at_end() {
            if self.peek() == b'\n' {
                self.line += 1
            }
            self.advance();
        }
        if self.is_at_end() {
            self.error_token("Unterminated string."); // TODO: in the book, this error includes line:       Lox.error(line, "Unterminated string.");
        } else {
            self.advance();
            self.add_token(
                TokenType::String{literal: self.source[self.start + 1..self.current - 1].to_string()},
            )
        }
    }

    pub fn next_char_matches(&mut self, expected_char: u8) -> bool {
        if self.is_at_end() {
            return false;
        }
        if expected_char == self.peek() {
            self.current += 1;
            true
        } else {
            false
        }
    }

    /// Should skip whitespace
    fn skip_whitespace(&mut self) {
        while !self.is_at_end() {
            match self.peek() {
                b' ' | b'\r' | b'\t' => {
                    self.advance();
                }
                b'\n' => {
                    self.line += 1;
                    self.advance();
                }
                _ => return,
            }
        }
    }

    /// Consumes the next character in the source file and returns it
    pub fn advance(&mut self) -> u8 {
        let char = self.peek();
        self.current += 1;
        char
    }

    /// Grabs the text of the current elxeme and creates a new token for it
    pub fn add_token(&mut self, token_type: TokenType) {
        let new_token = self.create_token(token_type);
        println!("{}", new_token);
        self.tokens.push(new_token)
    }

    /// Special token type for errors
    pub fn error_token(&mut self, message: &str) {
        let error_token = Token {
            token_type: TokenType::Error,
            lexeme: message.to_string(),
            line: self.line,
        };
        println!("{}", error_token);
        self.tokens.push(error_token)
    }

    pub fn identifier(&mut self) {
        while is_alpha_numeric(self.peek()) {
            self.advance();
        }
        let text = self.lexeme();

        let token_type: TokenType = KEYWORDS.get(text).cloned().unwrap_or(TokenType::Identifier);

        self.add_token(token_type)
    }

    fn peek(&mut self) -> u8 {
        if self.is_at_end() {
            b'\0'
        } else {
            self.source.as_bytes()[self.current]
        }
    }

    fn peek_next(&self) -> u8 {
        if self.current > self.source.len() - 2 {
            b'\0'
        } else {
            self.source.as_bytes()[self.current + 1]
        }
    }
}

fn is_digit(c: u8) -> bool {
    c.is_ascii_digit()
}

fn is_alpha(c: u8) -> bool {
    c.is_ascii_alphabetic() || c == b'_'
}

pub fn is_alpha_numeric(c: u8) -> bool {
    is_alpha(c) || is_digit(c)
}
