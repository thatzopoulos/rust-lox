# rust-lox

Rust Lox Interpreter

## Design Changes From Java Version
- Literal is not a part of scanner and is just embedded in the Enums for token's that require it (Number and String)